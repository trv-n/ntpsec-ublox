/*
 * refclock_ubx - clock driver for the ublox m8t and f9t timing receiver
 *
 * For detailed information on this driver, please refer to the
 * driver_ublox.html document accompanying the NTPsec distribution.
 *
 * Version 0.02; August 10, 2019
 * refer to driver_ublox.html for change log
 *
 * Copyright 2019 by the NTPsec project contributors
 * SPDX-License-Identifier: BSD-4-clause
 */

#include "config.h"

#ifdef HAVE_SYS_IOCTL_H
# include <sys/ioctl.h>
#endif /* not HAVE_SYS_IOCTL_H */

#if defined HAVE_SYS_MODEM_H
#include <sys/modem.h>
#endif

#include <termios.h>
#include <sys/stat.h>
#include <time.h>

#ifdef HAVE_SYS_IOCTL_H
#include <sys/ioctl.h>
#endif

#include "ntpd.h"
#include "ntp_io.h"
#include "ntp_refclock.h"
#include "ntp_stdlib.h"
#include "timespecops.h"
#include "gpstolfp.h"

/*
 * GPS Definitions
 */
#define	DESCRIPTION "UBLOX M8T"/* Long name */
#define NAME        "UBLOX"    /* shortname */
#define	PRECISION   (-20)      /* precision assumed (about 1 us) */
#define	REFID       "GPS\0"    /* reference ID */
#define UBX_MINPOLL 4          /* 16 seconds */
#define UBX_MAXPOLL 5          /* 32 seconds */
#define MIN_SAMPLES 7          /* minimum number of samples in the median filter to allow a poll */

/*
 * I/O Definitions
 */
#define	DEVICE   "/dev/ublox%d" /* device name and unit */
#define	SPEED232 B9600        /* uart speed (9600 bps) */

/* parse consts */
#define RMAX  1024
#define SYNC1 0xb5
#define SYNC2 0x62

/* parse states */
#define PARSED_EMPTY 0	
#define PARSED_SYNC1 1
#define PARSED_SYNC2 2
#define PARSED_CLASS 3
#define PARSED_ID    4
#define PARSED_LEN1  5
#define PARSING_DATA 6
#define PARSED_DATA  7
#define PARSED_CHKA  8
#define PARSED_FULL  9

/*
 * conversion defines from GPSD, TODO check licensing and remove before releasing if needed
 */
#define getsb(buf, off)	((int8_t)buf[off])
#define getub(buf, off)	((uint8_t)buf[off])
#define getles16(buf, off)	((int16_t)(((uint16_t)getub((buf),   (off)+1) << 8) | (uint16_t)getub((buf), (off))))
#define getleu16(buf, off)	((uint16_t)(((uint16_t)getub((buf), (off)+1) << 8) | (uint16_t)getub((buf), (off))))
#define getles32(buf, off)	((int32_t)(((uint16_t)getleu16((buf),  (off)+2) << 16) | (uint16_t)getleu16((buf), (off))))
#define getleu32(buf, off)	((uint32_t)(((uint16_t)getleu16((buf),(off)+2) << 16) | (uint16_t)getleu16((buf), (off))))
#define getles64(buf, off)	((int64_t)(((uint64_t)getleu32(buf, (off)+4) << 32) | getleu32(buf, (off))))
#define getleu64(buf, off)	((uint64_t)(((uint64_t)getleu32(buf, (off)+4) << 32) | getleu32(buf, (off))))

/*
 * unit control structure
 */
struct ubx_unit {
	unsigned int  unit;      /* NTP refclock unit number */
	unsigned int  samples;   /* samples in filter this poll */

	unsigned int  p_stat;    /* parser state */ 
	unsigned int  p_bytect;  /* parser byte counter */
	uint8_t       p_chka;    /* parser checksum 1 */
	uint8_t       p_chkb;    /* parser checksum 2 */
	uint8_t       msg_class; /* message class byte */
	uint8_t       msg_id;    /* message ID byte */
	uint16_t      msg_len;   /* message length */
	uint8_t       message[RMAX]; /* message data */

	bool          got_data;  /* got serial data this poll */
	bool          got_msg;   /* decoded a packet this poll */
	bool          got_time;	 /* got a time packet this poll */
	int           MCR;       /* modem control register value at startup */
	unsigned int  week;      /* GPS week number from TM2 message */
	uint32_t      tm2_tow;   /* UTC-adjusted GPS time of week from TM2 */
	uint32_t      nsec;      /* GPS nanoseconds of TOW from TM2 */
	uint32_t      acc;       /* accuracy estimate from TM2 */
	uint32_t      timels_tow;/* UTC time of week from TIMELS message */
	int           UTC_offset;/* GPS offset in seconds from TIMELS */
	bool          timels_ok; /* validate sequence of TM2 and TIMELS */
};

/*
 * Function prototypes
 */
static	bool	ubx_start    (int, struct peer *);
static	void	ubx_shutdown (struct refclockproc *);
static	void	ubx_poll     (int, struct peer *);
static	void 	ubx_io       (struct recvbuf *);
static	void	ubx_receive  (struct peer *);
static	bool	ubx_decode   (struct peer *);

/*
 * Transfer vector
 */
struct refclock refclock_ubx = {
	NAME,         /* basename of driver */
	ubx_start,    /* start up driver */
	ubx_shutdown, /* shut down driver */
	ubx_poll,     /* transmit poll message */
	NULL,         /* control - not used  */
	NULL,         /* initialize driver (not used) */
	NULL          /* called at 1Hz by mainloop (not used) */
};

/* Extract the clock type from the mode setting */
#define CLK_TYPE(x) ((int)(((x)->cfg.mode) & 0x7F))

/* enable informational message printing */
#define UIPRINT(x) \
 do { \
  if (pp->sloppyclockflag & CLK_FLAG2) \
    printf x; \
 } while (0)

/* enable error message printing */
#define UEPRINT(x) \
 do { \
  if (pp->sloppyclockflag & CLK_FLAG3) \
   printf x; \
 } while (0)

/*
 * ubx_start - open the devices and initialize data for processing
 */
static bool
ubx_start (
	int unit,
	struct peer *peer
	)
{
	struct ubx_unit *up;
	struct refclockproc *pp;
	int fd;
	struct termios tio;
	unsigned int cflag, iflag;
	char device[20], *path;

	pp = peer->procptr;
	pp->clockname = NAME;

	/* Open serial port. */
	if (peer->cfg.path)
	    path = peer->cfg.path;
	else {
	    int rcode;
	    snprintf(device, sizeof(device), DEVICE, unit);

	    /* build a path */
	    rcode = snprintf(device, sizeof(device), DEVICE, unit);
	    if ( 0 > rcode )
	        device[0] = '\0'; /* failed, set to NUL */
	    path = device;
        }
	fd = refclock_open(path, peer->cfg.baud ? 
	                   peer->cfg.baud : SPEED232, LDISC_RAW);
	if (0 > fd) {
	        msyslog(LOG_ERR, "REFCLOCK: %s device open(%s) failed",
			refclock_name(peer), path);
		return false;
	}

	/* Allocate and initialize unit structure */
	up = emalloc_zero(sizeof(*up));
	LOGIF(CLOCKINFO, (LOG_NOTICE, "%s open at %s",
			  refclock_name(peer), path));
	if (tcgetattr(fd, &tio) < 0) {
		msyslog(LOG_ERR, "REFCLOCK: %s tcgetattr failed: %m",
		        refclock_name(peer));
		goto init_error;
	}

	pp->disp = 500 * S_PER_NS; /* extra ~500ns for serial port delay */

	tio.c_cflag = (CS8|CLOCAL|CREAD);
	tio.c_iflag &= (unsigned)~ICRNL;
	cflag = tio.c_cflag;
	iflag = tio.c_iflag;
	if (tcsetattr(fd, TCSANOW, &tio) == -1 || tcgetattr(fd, &tio) == -1 ||
	    tio.c_cflag != cflag || tio.c_iflag != iflag) {
		msyslog(LOG_ERR, "REFCLOCK: %s tcsetattr failed: wanted cflag 0x%x got 0x%x, wanted iflag 0x%x got 0x%x, return: %m",
		        refclock_name(peer), cflag, (unsigned int)tio.c_cflag,
		        iflag, (unsigned int)tio.c_iflag);
		goto init_error;
	}

	 /* idle RTS at positive voltage (RS232 to CMOS is inverting)*/
	if (ioctl(fd, TIOCMGET, &up->MCR) < 0) {
		msyslog(LOG_ERR, "REFCLOCK: %s TIOCMGET failed: %m",
		        refclock_name(peer));
		goto init_error;
	}
	up->MCR |= TIOCM_RTS;
	if (ioctl(fd, TIOCMSET, &up->MCR) < 0 ||
	    !(up->MCR & TIOCM_RTS)) {
		msyslog(LOG_ERR, "REFCLOCK: %s TIOCMSET failed: MCR=0x%x, return=%m",
		        refclock_name(peer), (unsigned int)up->MCR);
		goto init_error;
	}

	pp->io.clock_recv = ubx_io;
	pp->io.srcclock = peer;
	pp->io.datalen = 0;
	pp->io.fd = fd;
	if (!io_addclock(&pp->io)) {
		msyslog(LOG_ERR, "%s io_addclock failed", refclock_name(peer));
		goto init_error;
	}

	/* Initialize miscellaneous variables */
	pp->unitptr = up;
	pp->clockdesc = DESCRIPTION;
	pp->leap = LEAP_NOWARNING; /* TODO add later */
	
	peer->precision = PRECISION;
	peer->sstclktype = CTL_SST_TS_UHF;
	peer->cfg.minpoll = UBX_MINPOLL;
	peer->cfg.maxpoll = UBX_MAXPOLL;
	memcpy((char *)&pp->refid, REFID, REFIDLEN);

	up->unit = (short) unit;
	up->p_stat = PARSED_EMPTY;
	up->p_bytect = 0;
	up->timels_ok = false;
	return true;

init_error:
	close(fd);
	pp->io.fd = -1;
	free(up);
	return false;
}

/*
 * ubx_shutdown - shut down the clock
 */
static void
ubx_shutdown (
	struct refclockproc *pp
	)
{
	struct ubx_unit * const up = (struct ubx_unit *)pp->unitptr;

	if (up != NULL) {
		free(up);
	}
	pp->unitptr = (void *)NULL;
	if (-1 != pp->io.fd)
		io_closeclock(&pp->io);
	pp->io.fd = -1;
}

/*
 * ubx_decode - decode ublox messages
 */
static bool
ubx_decode (
	struct peer *peer
	)
{
	struct refclockproc * const pp = peer->procptr;
	struct ubx_unit * const up = (struct ubx_unit *)pp->unitptr;

	int cid;
	uint8_t *d = up->message;

	cid = (up->msg_class << 8) | up->msg_id;
	switch (cid) {
	case 0x0d03: { /* UBX_TIM_TM2 */
		const char timeBases[4][8] = {"LOCAL", "GPS", "UTC", "INVALID"};
		const char modes[2][8] = {"SINGLE", "RUNNING"};
		const char runStates[2][8] = {"ARMED", "STOPPED"};

		const unsigned char ch = d[0];
		const unsigned char mode = d[1] & 0x01;
		const unsigned char run = (d[1] & 0x02) >> 1;
		const unsigned char newFallingEdge = (d[1] & 0x04) >> 2;
		const unsigned char timeBase = (d[1] & 0x18) >> 3;
		const unsigned char utcAvailable = (d[1] & 0x20) >> 5;
		const unsigned char timeValid = (d[1] & 0x40) >> 6;
		const unsigned char newRisingEdge = (d[1] & 0x80) >> 7;
		const unsigned int count = getleu16(d, 2);
		const unsigned int wnR = getleu16(d, 4);
		const unsigned int wnF = getleu16(d, 6);
		const uint32_t towMsR = getleu32(d, 8);
		const uint32_t towSubMsR = getleu32(d, 12);
		const uint32_t towMsF = getleu32(d, 16);
		const uint32_t accEst = getleu32(d, 24);
		
		uint32_t secR, secF, frac, adj_w;

		UIPRINT(("ubx_decode: unit %u: TM, wR=%u, tR=%u, wF=%u, tF=%u newR=%u, newF=%u, ac=%u, ct=%u, ch=%u, mode=%s, run=%s, tBase=%s, utcAv=%u, tVld=%u\n",
			up->unit, wnR, towMsR, wnF, towMsF, newRisingEdge, newFallingEdge, accEst, count, ch, modes[mode], runStates[run], timeBases[timeBase], utcAvailable, timeValid));

		if (timeBase != 2) {
			UEPRINT(("ubx_decode: unit %u: TM2: timeBase is not UTC, skipping\n", up->unit));
			return false;
		}
		if (accEst > 1000) {
			UEPRINT(("ubx_decode: unit %u: TM2: accuracy estimate more than 1000ns (%u), skipping\n", up->unit, accEst));
			return false;
		}
		
		if (!up->timels_ok) {
			UEPRINT(("ubx_decode: unit %u: TM2: got TM2 without corresponding TIMELS, skipping\n", up->unit));
			return false;
		}

		if (!newFallingEdge || !newRisingEdge) {
			UEPRINT(("ubx_decode: unit %u: TM2: one or more stale edges (R=%u,F=%u, skipping\n",
			       up->unit, newRisingEdge, newFallingEdge));
			return false;
		}


		secR = towMsR / 1000;
		adj_w = wnR;
		secF = towMsF / 1000;
		if (secR != secF) {
			UEPRINT(("ubx_decode: unit %u: TM2: rising and falling edges are not in the same second (R=%u,F=%u, skipping\n",
			       up->unit, secR, secF));
			return false;
		}

		secR += up->UTC_offset;
		if (secR > SECSPERWEEK) {
			secR = secR % SECSPERWEEK;
			adj_w++;
		}
		if (up->timels_tow != secR) {
			UEPRINT(("ubx_decode: unit %u: TM2: mismatch between TM2 TOW (%u) and TIMELS TOW (%u), skipping\n",
			       up->unit, secR, up->timels_tow));
			return false;
		}

		frac = (towMsR % 1000) * NS_PER_MS;
		frac += towSubMsR;

		up->week = adj_w;
		up->tm2_tow = secR;
		up->nsec = frac;
		up->acc = accEst;
		up->got_time = true;
		return true;
		break;
	}
	case 0x0126: { /* UBX_NAV_TIMELS */
		const struct timespec pulse_delay = {0, 50 * NS_PER_MS};
		const struct timespec pulse_len = {0, 10 * NS_PER_MS};
		const char sources_curr[10][16] = {"default", "GPS-GLO diff", "GPS", "SBAS", "BEIDOU", "GALILEO", "AIDED", "CONFIGURED", "unknown", "(INVALID)"};
		const char sources_next[8][16] = {"no source", "(INVALID)", "GPS", "SBAS", "BEIDOU", "GALILEO", "GLONASS", "(INVALID)"};

		const uint32_t iTOW = getleu32(d, 0);
		const unsigned char ver = d[4];
		const int currLs = getsb(d, 9);
		const int lsChange = getsb(d, 11);
		const int32_t timeToLsEvent = getles32(d, 12);
		const unsigned int wnOfNextLs = getleu16(d, 16);
		const unsigned int dnOfNextLs = getleu16(d, 18);
		const unsigned char currLsIsValid = d[23] & 0x01;
		const unsigned char nextLsIsValid = (d[23] & 0x02) >> 1;

		unsigned char srcOfCurrLs = d[8];
		unsigned char srcOfLsChange = d[10];

		uint32_t sec;
		
		if (srcOfCurrLs == 255)
			srcOfCurrLs = 8;
		else if (srcOfCurrLs > 7)
			srcOfCurrLs = 9;

		if (srcOfLsChange > 6)
			srcOfLsChange = 6;

		UIPRINT(("ubx_decode: unit %u: LS, iTOW=%u, ver=%u, currLs=%d, lsChg=%d, ttoLSe=%d, wNext=%u, dNext=%u, cValid=%u, nValid=%u, csrc=%s, nsrc=%s\n",
			up->unit, iTOW, ver, currLs, lsChange, timeToLsEvent, wnOfNextLs, dnOfNextLs, currLsIsValid, nextLsIsValid, sources_curr[srcOfCurrLs], sources_next[srcOfLsChange]));
		
		if (!currLsIsValid || srcOfCurrLs > 7) {
			UEPRINT(("ubx_decode: unit %u: LS: current source unknown or invalid\n", up->unit));
			return false;
		}
		sec = iTOW / 1000; /* almost always 1000ms, a few times a day will be 999ms */
		
		/*
		 * wait after reception of TIMELS. 100 to 150ns of additional
		 * jitter has been observed if there is no wait or a wait of
		 * 20ms here, so wait more than 20ms. Triggering should occur
		 * with a delay of at least 170 milliseconds after the top of 
		 * the second (offset will show in the timecode part of the
		 * "ubx_receive: xxxx offset is xxxx for timecode xxxx"
		 * line printed when driver fudge flag2 is 1) for the M8T,
		 * and at least 140 milliseconds for the F9T
		 */
		nanosleep(&pulse_delay, NULL);

		/* warm-up */
		up->MCR |= TIOCM_RTS;
		IGNORE(ioctl(pp->io.fd, TIOCMSET, &up->MCR));
		get_systime(&pp->lastrec);

		/* raise EXTINT (RS232 voltage from positive to negative) */
		up->MCR &= ~TIOCM_RTS;
		IGNORE(ioctl(pp->io.fd, TIOCMSET, &up->MCR));

		/* get timestamp after triggering since RAND_bytes is slow */
		get_systime(&pp->lastrec);

		/* hold high to help distingush pulse polarity */
		nanosleep(&pulse_len, NULL);

		/* back to idle state (positive RS232 voltage) */
		up->MCR |= TIOCM_RTS;
		IGNORE(ioctl(pp->io.fd, TIOCMSET, &up->MCR));

		up->timels_tow = sec;
		up->UTC_offset = currLs;
		up->timels_ok = true;

		break;
	}
	}
	return false;
}

/*
 * ubx_receive - process complete ublox messages created by ubx_io
 */
static void
ubx_receive (
	struct peer * peer
	)
{
	struct ubx_unit *up;
	struct refclockproc *pp;
	struct calendar date;

	pp = peer->procptr;
	up = pp->unitptr;

	/*
	 * Wait for fudge flags to initialize. Also, startup may have caused
	 * a spurious edge, so wait until the first poll is complete
	 */
	if (pp->polls < 1)
		return;

	up->got_msg = true;
	if (!ubx_decode(peer))
		return;

	/* add sample to filter */
	gpstocal(up->week, up->tm2_tow, up->UTC_offset, &date);
	pp->lastref = pp->lastrec;
	pp->year = date.year;
	pp->day = date.yearday;
	pp->hour = date.hour;
	pp->minute = date.minute;
	pp->second = date.second;
	pp->nsec = up->nsec;
	if (refclock_process(pp)) {
		UIPRINT(("ubx_receive: unit %u: offset is %.9f for timecode %4d %03d %02d:%02d:%02d.%09ld\n",
		       up->unit, pp->filter[pp->coderecv], pp->year, pp->day,
		       pp->hour, pp->minute, pp->second, pp->nsec));
		up->samples++;
	} else {
		refclock_report(peer, CEVNT_BADTIME);
		UEPRINT(("ubx_receive: unit %u: failed to process timecode %4d %03d %02d:%02d:%02d.%09ld\n",
		       up->unit, pp->year, pp->day, pp->hour, pp->minute,
		       pp->second, pp->nsec));
	}
}

/*
 * ubx_poll - called by the transmit procedure
 */
static void
ubx_poll (
	int unit,
	struct peer *peer
	)
{
	struct ubx_unit *up;
	struct refclockproc *pp;
	int cl, smpct;
	bool err;

	UNUSED_ARG(unit);

	pp = peer->procptr;
	up = pp->unitptr;

	/* samples are not taken until second poll */
	if (++pp->polls < 2)
		return;

	/* check status for the previous poll interval */
	smpct = up->samples;
	err = (smpct < MIN_SAMPLES);
	if (err) {
		refclock_report(peer, CEVNT_TIMEOUT);
		if (!up->got_data) {
			UEPRINT(("ubx_poll: unit %u: no data from serial port seen this poll interval\n",
			       up->unit));
		} else if (!up->got_msg) {
			UEPRINT(("ubx_poll: unit %u: serial data seen, but no messages could be decoded from it this poll interval\n",
			       up->unit));
		} else if (!up->got_time) {
			UEPRINT(("ubx_poll: unit %u: message(s) decoded this poll interval, but no TM2 messages were seen. Check your RS232 RTS line\n",
			       up->unit));
		} else {
			UEPRINT(("ubx_poll: unit %u: not enough samples (%u, min %d), skipping poll\n",
			       up->unit, up->samples, MIN_SAMPLES));
			pp->codeproc = pp->coderecv; /* reset filter */
		}
	}
	up->got_data = false;
	up->got_msg = false;
	up->got_time = false;
	up->samples = 0;
	if (err)
		return;

	/* process samples in filter */
	refclock_receive(peer);

	/* record filter results in clockstats */
	cl = snprintf(pp->a_lastcode, sizeof(pp->a_lastcode),
	              "%d,%.9f,%.9f,%.9f,%u",
	              smpct, pp->offset, pp->disp, pp->jitter, up->acc);
	pp->lencode = (cl < (int)sizeof(pp->a_lastcode)) ? cl : 0;
	record_clock_stats(peer, pp->a_lastcode);

	UIPRINT(("ubx_poll: unit %u: samples=%d offset=%.9f disp=%.9f jitter=%.9f\n",
	       up->unit, smpct, pp->offset, pp->disp, pp->jitter));
}

/*
 * ubx_io - create ublox messages from serial data stream
 */
static void
ubx_io (
	struct recvbuf *rbufp
	)
{
	struct ubx_unit *up;
	struct refclockproc *pp;
	struct peer *peer;

	uint8_t * c, * d;

	peer = rbufp->recv_peer;
	pp = peer->procptr;
	up = pp->unitptr;
	up->got_data = true;

	c = (uint8_t *) &rbufp->recv_buffer;
	d = c + rbufp->recv_length;
	while (c != d) {
		switch (up->p_stat) {
		case PARSED_SYNC1:
			if (SYNC2 == *c)
				up->p_stat = PARSED_SYNC2;
			else
				up->p_stat = PARSED_EMPTY;
			break;

		case PARSED_SYNC2:
			up->msg_class = *c;
			up->p_chka = *c;
			up->p_chkb = *c;
			up->p_stat = PARSED_CLASS;
			break;

		case PARSED_CLASS:
			up->msg_id = *c;
			up->p_chka += *c;
			up->p_chkb += up->p_chka;
			up->p_stat = PARSED_ID;
			break;

		case PARSED_ID:
			up->msg_len = *c; /*lsb*/
			up->p_chka += *c;
			up->p_chkb += up->p_chka;
			up->p_stat = PARSED_LEN1;
			break;

		case PARSED_LEN1:
			up->msg_len |= (*c << 8);
			up->p_chka += *c;
			up->p_chkb += up->p_chka;
			up->p_bytect = 0;
			up->p_stat = PARSING_DATA;
			break;

		case PARSING_DATA:
			up->message[up->p_bytect++] = *c;
			up->p_chka += *c;
			up->p_chkb += up->p_chka;

			if (up->p_bytect > RMAX - 1) {
				UEPRINT(("ubx_io: unit %u: oversize serial message (%uB) 0x%02x discarded\n",
				       up->unit, up->p_bytect, up->msg_id));
				up->p_stat = PARSED_EMPTY;
			} else if (up->p_bytect >= up->msg_len)
				up->p_stat = PARSED_DATA;
			break;

		case PARSED_DATA:
			if (up->p_chka == *c)
				up->p_stat = PARSED_CHKA;
			else
				up->p_stat = PARSED_EMPTY;
			break;

		case PARSED_CHKA:
			if (up->p_chkb == *c) {
				ubx_receive(peer);
				up->p_stat = PARSED_FULL;
			} else
				up->p_stat = PARSED_EMPTY;
			break;

		default: /*PARSED_FULL or PARSED_EMPTY*/
			if (SYNC1 == *c) {
				up->p_stat = PARSED_SYNC1;
			} else
				up->p_stat = PARSED_EMPTY;
			break;
		}
		c++;
	} /* while chars in buffer */
}